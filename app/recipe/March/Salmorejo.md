# Recipe Title

Salorejo cordobes.

Source: http://www.lostragaldabas.net/salmorejo-cordobes/

## Ingredients

    1kg de tomates pera bien maduritos
    200gr de pan de telera o en su defecto pan de masa blanca
    100gr de aceite de oliva virgen extra
    1 diente de ajo
    1 cucharadita de sal
    Huevo duro
    Jamón serrano

# Instructions

    1. Pelamos los tomates y los ponemos en el vaso de nuestra batidora junto con el ajo, trituramos hasta que quede sin grumos.
    2. Añadimos el pan, el aceite y la sal y  volvemos a poner en marcha nuestra batidora hasta conseguir una masa homogénea y lisa. No tengas prisa esto requiere su tiempo.
    3. Metemos en el frigorífico y dejamos enfriar.
    4. Colocamos en un bol y decoramos con jamón serrano del bueno, huevo duro picadito y un chorreoncito de aceite de oliva
